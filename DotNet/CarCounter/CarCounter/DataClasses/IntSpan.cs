﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarCounter.DataClasses
{
    public class IntSpan
    {
        public int From;
        public int To;

        public IntSpan(int from, int to)
        {
            this.From = from;
            this.To = to;
        }
    }
}
