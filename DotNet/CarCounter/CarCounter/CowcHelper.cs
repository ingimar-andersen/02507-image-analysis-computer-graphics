﻿using CarCounter.DataClasses;
using Emgu.CV;
using Emgu.CV.Structure;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CarCounter
{
    class CowcHelper
    {
        public enum PredictionCategory
        {
            CarNegative = 0,
            CarPositive = 1,
            Grass = 2,
            Trees = 3,
            Roads = 4,
        }

        //private const string DATA_FOLDERPATH = "C:\\Users\\grena\\OneDrive\\DTU\\2017 - Haust\\02507 Image Analysis and Computer Graphics\\Data";
        private const string OUTPUT_FOLDERPATH = "C:\\CarCounterData\\Output";
        private const string COWCDATA_FOLDERPATH = "C:\\CarCounterData\\CowcData";

        private const string ORIGINAL_FILENAME = "top_potsdam_5_10_RGB.png";
        private const string ANNOTADED_CARS_FILENAME = "top_potsdam_5_10_RGB_Annotated_Cars.png";
        public static string NEGATIVES_FILENAME = "top_potsdam_5_10_RGB_Annotated_Negatives.png";

        //For test batch
        private const string ORIGINAL_TEST_FILENAME = "top_potsdam_2_13_RGB.png";
        private const string ANNOTADED_CARS_TEST_FILENAME = "top_potsdam_2_13_RGB_Annotated_Cars.png";
        public static string NEGATIVES_TEST_FILENAME = "top_potsdam_2_13_RGB_Annotated_Negatives.png";

        public const double GSD = 0.15; // Ground sample distance [m]
        public const double CAR_SIZE = 3; // Car rectangle size [m]

        private static int? _carPixelSize;
        public readonly Dictionary<string, string> OriginalsByFilename;
        public readonly Dictionary<string, string> AnnoCarsByFilename;
        public readonly Dictionary<string, string> AnnoNegsByFilename;

        public static IntSpan DefaultBrightnessSpan = new IntSpan(-15, 15);
        public static IntSpan DefaultContrastPercentageSpan = new IntSpan(85, 115);

        public static int CarPixelSize
        {
            get
            {
                if (_carPixelSize == null)
                {
                    _carPixelSize = (int)Math.Round(CAR_SIZE / GSD);
                }
                return _carPixelSize.Value;
            }
        }

        public bool IsTestData { get; }
        public string BaseOutputPath { get; }

        public CowcHelper(bool isTestData)
        {
            IsTestData = isTestData;
            BaseOutputPath = Path.Combine(COWCDATA_FOLDERPATH, isTestData ? "Testing" : "Training");
            if (!Directory.Exists(BaseOutputPath))
            {
                Directory.CreateDirectory(BaseOutputPath);
            }
            //All dictionary have the same keys
            OriginalsByFilename = Directory.GetFiles(Path.Combine(BaseOutputPath, "Original")).ToDictionary(x => Path.GetFileNameWithoutExtension(x));
            AnnoCarsByFilename = Directory.GetFiles(Path.Combine(BaseOutputPath, "AnnotadedCars")).ToDictionary(x => GetShortFileName(x));
            AnnoNegsByFilename = Directory.GetFiles(Path.Combine(BaseOutputPath, "AnnotadedNegatives")).ToDictionary(x => GetShortFileName(x));
            if (!VerifyDicts(OriginalsByFilename, AnnoCarsByFilename, AnnoNegsByFilename))
            {
                MessageBox.Show("Dictionaries could not be verified.");
            }
        }

        public void CreateDataSetNumpy(bool onlyPositives)
        {
            var positive = onlyPositives ? "_pos" : "";
            var outputPath = Path.Combine(OUTPUT_FOLDERPATH, IsTestData ? $"test_data{positive}.bin": $"train_data{positive}.bin");
            if (File.Exists(outputPath))
            {
                var result = MessageBox.Show($"Do you want to overwrite {outputPath} ?", "Overwrite?", MessageBoxButton.YesNo);
                if (result == MessageBoxResult.No) return;
                else
                {
                    File.Delete(outputPath);
                }
            }
          
            var failedKeyValues = TryCreateTrainSetNumpy(outputPath, OriginalsByFilename, onlyPositives);

            while (failedKeyValues.Any())
            {
                foreach (var kv in failedKeyValues)
                {
                    Console.WriteLine(kv.Value);
                }
                var result = MessageBox.Show($"{failedKeyValues.Count:0} images failed. Try Again?", "Try again?", MessageBoxButton.YesNo);
                if (result == MessageBoxResult.No) break;

                failedKeyValues = TryCreateTrainSetNumpy(outputPath, failedKeyValues, onlyPositives);
            }
        }

        private List<KeyValuePair<string,string>> TryCreateTrainSetNumpy(string outputPath, IEnumerable<KeyValuePair<string, string>> originalKeyValues, bool onlyPositives = true)
        {
            float count = 0;
            var failedKeyValues = new List<KeyValuePair<string, string>>();
            foreach (var keyValue in originalKeyValues)
            {
                count++;
                Console.WriteLine($"Started processing {keyValue} @ {DateTime.Now.ToLongTimeString()}");
                Image<Rgb, byte> original = null;
                try
                {
                    original = new Image<Rgb, byte>(keyValue.Value);
                }
                catch (Exception)
                {
                    Console.WriteLine($"Could not initialize image {keyValue.Value}");
                    failedKeyValues.Add(keyValue);
                    continue;
                }

                List<System.Drawing.Point> carPositions = null;
                List<System.Drawing.Point> negsPositions = null;
                if (AnnoCarsByFilename.TryGetValue(keyValue.Key, out string annoCarsPath))
                {
                    carPositions = ParseCarAnnotation(annoCarsPath);
                }

                if (!onlyPositives && AnnoNegsByFilename.TryGetValue(keyValue.Key, out string annoNegsPath))
                {
                    negsPositions = ParseCarAnnotation(annoNegsPath);
                }

                if (carPositions == null || (negsPositions == null && !onlyPositives))
                {
                    Console.WriteLine($"Positions could not be found due to missing key: {keyValue}");
                    original.Dispose();
                    continue;
                }
                Console.WriteLine($"Parsing complete @ {DateTime.Now.ToLongTimeString()}");
                AppendCroppedImageBytes(outputPath, original, carPositions, label: 0x00);
                Console.WriteLine($"Cars cropped @ {DateTime.Now.ToLongTimeString()}");

                if (!onlyPositives)
                {
                    AppendCroppedImageBytes(outputPath, original, negsPositions, label: 0x01);
                    Console.WriteLine($"Negs cropped @ {DateTime.Now.ToLongTimeString()}");
                }
                Console.WriteLine($"{count / OriginalsByFilename.Count() * 100:0.00}% complete");
                original.Dispose();
            }
            return failedKeyValues;
        }

        private static bool VerifyDicts(Dictionary<string, string> originalsByFilename, Dictionary<string, string> annoCarsByFilename, Dictionary<string, string> annoNegsByFilename)
        {
            if (originalsByFilename.Count != annoNegsByFilename.Count || originalsByFilename.Count != annoNegsByFilename.Count) return false;
            foreach (var key in originalsByFilename.Keys)
            {
                if (!annoCarsByFilename.ContainsKey(key)) return false;
                if (!annoNegsByFilename.ContainsKey(key)) return false;
            }
            return true;
        }

        private static string GetShortFileName(string path)
        {
            var filename = Path.GetFileNameWithoutExtension(path);
            return filename.Substring(0, filename.LastIndexOf("_Annotated"));
        }

        public static Image<Rgb, float> GetImageWithCarPos()
        {
            var original = new Image<Rgb, float>(Path.Combine(COWCDATA_FOLDERPATH, ORIGINAL_FILENAME));
            var carPositions = ParseCarAnnotation(Path.Combine(COWCDATA_FOLDERPATH, ANNOTADED_CARS_FILENAME));
            var negativePositions = ParseCarAnnotation(Path.Combine(COWCDATA_FOLDERPATH, NEGATIVES_FILENAME));
            var result = DrawLocations(original, carPositions, Color.Green, Color.Black);

            return DrawLocations(result, negativePositions, Color.Red, Color.Black);
        }
 

        public void OutputCroppedImages(bool useRandomDistortions, bool onlyPositives)
        {
            foreach (var keyValue in OriginalsByFilename)
            {
                using (var original = new Image<Rgb, float>(keyValue.Value))
                {
                    List<System.Drawing.Point> carPositions = null;
                    List<System.Drawing.Point> negsPositions = null;
                    if (AnnoCarsByFilename.TryGetValue(keyValue.Key, out string annoCarsPath))
                    {
                        carPositions = ParseCarAnnotation(annoCarsPath);
                    }

                    if (!onlyPositives && AnnoNegsByFilename.TryGetValue(keyValue.Key, out string annoNegsPath))
                    {
                        negsPositions = ParseCarAnnotation(annoNegsPath);
                    }

                    if (carPositions == null || (negsPositions == null && !onlyPositives))
                    {
                        Console.WriteLine($"Positions could not be found due to missing key: {keyValue}");
                        continue;
                    }

                    ExportCroppedImages(original, carPositions, $"Positives{CarPixelSize}x{CarPixelSize}", "pos", useRandomDistortions);
                    if (!onlyPositives)
                    {
                        ExportCroppedImages(original, negsPositions, $"Negatives{CarPixelSize}x{CarPixelSize}", "neg", useRandomDistortions);
                    }
                }
            }
        }

        private static Image<Rgb, float> DrawLocations(Image<Rgb, float> original, List<System.Drawing.Point> locations, Color boxColor, Color crossColor)
        {
            var result = original.Clone();
            var offset = (int)Math.Round(CarPixelSize / 2.0);
            var crossSize = (int)Math.Ceiling(CarPixelSize / 4.0);
 
            foreach (var pos in locations)
            {
                var rectangle = new Rectangle(pos.X - offset, pos.Y - offset, CarPixelSize, CarPixelSize);
                var originalRect = new Rectangle(0, 0, original.Width, original.Height);
                result.Draw(rectangle, new Rgb(boxColor), 1);
                result.Draw(new Cross2DF(new PointF(pos.X, pos.Y), crossSize, crossSize), new Rgb(crossColor), 1);
                
            }
            return result;
        }

        private static void AppendCroppedImageBytes(string outputPath, Image<Rgb, byte> original, List<System.Drawing.Point> locations, byte label = 0x0)
        {
            var offset = (int)Math.Round(CarPixelSize / 2.0);
            foreach (var pos in locations)
            {
                var rectangle = new Rectangle(pos.X - offset, pos.Y - offset, CarPixelSize, CarPixelSize);
                var originalRect = new Rectangle(0, 0, original.Width, original.Height);
                try
                {
                    if (!originalRect.Contains(rectangle))
                    {
                        Console.WriteLine($"Rectangle out of bounds: {rectangle.ToString()}");
                        continue;
                    }
                    using (var subrect = original.GetSubRect(rectangle))
                    {
                        var jpgData = subrect.ToJpegData(100); //No compression
                        var lenghtInBytes = BitConverter.GetBytes(jpgData.Length);
                        // Label | Jpg length
                        //  xx   | xx xx xx xx
                        var labelBa = new byte[] { label };
                        IOTools.AppendAllBytes(outputPath, labelBa.Concat<byte>(lenghtInBytes).ToArray());
                        IOTools.AppendAllBytes(outputPath, jpgData); 
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        private int _exportCount = 0;
        private void ExportCroppedImages(Image<Rgb, float> original, List<System.Drawing.Point> locations, string folderName, string fileNameSuffix = "", bool useRandomDistortions = true)
        {
            var offset = (int)Math.Round(CarPixelSize / 2.0);
            var outputFolderPath = Path.Combine(BaseOutputPath, folderName);
            if (!Directory.Exists(outputFolderPath))
            {
                Directory.CreateDirectory(outputFolderPath);
            }
            var random = new Random();
            foreach (var pos in locations)
            {
                var rectangle = new Rectangle(pos.X - offset, pos.Y - offset, CarPixelSize, CarPixelSize);
                var originalRect = new Rectangle(0, 0, original.Width, original.Height);
                try
                {
                    _exportCount++;
                    var suffix = String.IsNullOrWhiteSpace(fileNameSuffix) ? "" : "_" + fileNameSuffix;
                    if (!originalRect.Contains(rectangle))
                    {
                        Console.WriteLine($"Rectangle out of bounds: {rectangle.ToString()}");
                        continue;
                    }
                    var subRect = original.GetSubRect(rectangle);
                    if (useRandomDistortions)
                    {
                        subRect = ImageUtils.ApplyRandomDistortions(subRect);
                    }
                    var filename = Path.Combine(outputFolderPath, $"{_exportCount:00000000}{suffix}.png");
                    subRect.Save(filename);

                    subRect = subRect.Rotate90();
                    if (useRandomDistortions)
                    {
                        subRect = ImageUtils.ApplyRandomDistortions(subRect);
                    }
                    filename = Path.Combine(outputFolderPath, $"{_exportCount:00000000}{suffix}90deg.png");
                    subRect.Save(filename);

                    subRect = subRect.Rotate90();
                    if (useRandomDistortions)
                    {
                        subRect = ImageUtils.ApplyRandomDistortions(subRect);
                    }
                    filename = Path.Combine(outputFolderPath, $"{_exportCount:00000000}{suffix}180deg.png");
                    subRect.Save(filename);

                    subRect = subRect.Rotate90();
                    if (useRandomDistortions)
                    {
                        subRect = ImageUtils.ApplyRandomDistortions(subRect);
                    }
                    filename = Path.Combine(outputFolderPath, $"{_exportCount:00000000}{suffix}270deg.png");
                    subRect.Save(filename);

                    subRect.Dispose();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        public static List<System.Drawing.Point>ParseCarAnnotation(string filePath)
        {
            using (var image = new Image<Gray, byte>(filePath))
            {
                var carPositions = new List<System.Drawing.Point>();

                for (int x = 0; x < image.Width; x++)
                {
                    for (int y = 0; y < image.Height; y++)
                    {
                        if (image[y, x].Intensity > 0)
                        {
                            carPositions.Add(new System.Drawing.Point(x, y));
                        }
                    }
                }
                return carPositions;
            }
        }

        public static void SplitIntoSegments(Image<Rgb, float> sourceImage, System.Drawing.Size segmentSize, string outputPath, string baseFileName = "", int? skippingHorizontal = null, int? skippingVertical = null, bool createDictionary = false, bool useRandomDistortions = false, IntSpan brightnessDistSpan = null, IntSpan contrastSpanPercentage = null)
        {
            Console.WriteLine($"Segmenting picture into folder: {outputPath}");
            if (segmentSize.Height > sourceImage.Height || segmentSize.Width > sourceImage.Width)
                throw new Exception("Segment larger than source image");
    
            var outputImage = sourceImage.Clone();
            int skipX = skippingHorizontal != null ? skippingHorizontal.Value : segmentSize.Width;
            int skipY = skippingVertical != null ? skippingVertical.Value : segmentSize.Height;

            var totalIterations = ((1.0 * (sourceImage.Width - segmentSize.Width) / skipX) * (1.0 * (sourceImage.Height - segmentSize.Height)/ skipY));
            var counter = 0;
            var mapping = new Dictionary<int, System.Drawing.Point>();
            for (int x = 0; x < sourceImage.Width - segmentSize.Width; x += skipX)
            {
                for (int y = 0; y < sourceImage.Height - segmentSize.Height; y += skipY)
                {
                    counter++;
                    var fileName = Path.Combine(outputPath, $"{baseFileName}_{counter:00000000}.png");
                    var location = new System.Drawing.Point(x, y);
                    var rect = new Rectangle(location, segmentSize);

                    var segment = sourceImage.GetSubRect(rect);
                    if (useRandomDistortions)
                    {
                        segment = ImageUtils.ApplyRandomDistortions(segment, brightnessDistSpan, contrastSpanPercentage);
                    }
                    segment.Save(fileName);
                    segment.Dispose();

                    mapping.Add(counter, location);
                    if(counter % 1000 == 0)
                    {
                        var progress = counter / totalIterations*100;
                        Console.WriteLine($"{progress:0.00}%");
                    }
                }
            }
            Console.WriteLine("100%");
            if (createDictionary)
            {
                var dictionaryPath = Path.Combine(outputPath, "dictionary_int_Point.txt");
                Console.WriteLine($"Creating dictionary in path: {dictionaryPath}");
                File.WriteAllText(dictionaryPath, JsonConvert.SerializeObject(mapping));
            }
        }

        public static Dictionary<int, System.Drawing.Point> ParsePointDictionary(string filePath)
        {
            var jsonString = File.ReadAllText(filePath);
            return JsonConvert.DeserializeObject<Dictionary<int, System.Drawing.Point>>(jsonString);
        }

        public static List<Tuple<int, double>> ParsePredictions(string filePath)
        {
            var result = new List<Tuple<int, double>>(); // category, prediction probability
            var lines = File.ReadAllLines(filePath);
            foreach(var l in lines)
            {
                var values = l.Split(' ');
                result.Add(new Tuple<int, double>(int.Parse(values[0]), double.Parse(values[1], CultureInfo.InvariantCulture)));
            }
            return result;
        }
    }
}
