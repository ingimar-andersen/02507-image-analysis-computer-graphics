﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CarCounter
{
    class IOTools
    {
        public static void AppendAllBytes(string path, byte[] bytes)
        {
            //argument-checking here.

            using (var stream = new FileStream(path, FileMode.Append))
            {
                stream.Write(bytes, 0, bytes.Length);
            }
        }

        internal static void CleanOrCreateDirectory(string dirPath)
        {
            if (Directory.Exists(dirPath) && Directory.GetFiles(dirPath).Any())
            {
                var msgBoxResult = MessageBox.Show("Output folder is not empty. Delete files inside?", "Delete?", MessageBoxButton.YesNo);
                if (msgBoxResult == MessageBoxResult.Yes)
                {
                    Directory.Delete(dirPath, true);
                    Directory.CreateDirectory(dirPath);
                }
                else
                {
                    return;
                }
            }
            else
            {
                Directory.CreateDirectory(dirPath);
            }
        }
    }
}
