﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarCounter
{
    public class PredictionSegment
    {
        public readonly double Probability;
        public readonly Rectangle BoundingBox;
        public bool? TruePositive = null;

        public PredictionSegment(double probability, Rectangle boundingBox)
        {
            this.Probability = probability;
            this.BoundingBox = boundingBox;
        }
    }
}
