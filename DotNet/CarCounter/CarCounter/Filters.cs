﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CarCounter.FilterDefinitions;

namespace CarCounter
{
    public class Filters
    {
        ///<summary>
        ///<para>l</para>
        ///<param name="nKernel">
        ///<para>Sets the dimensions of the kernel. Dimensions = 2*n + 1</para>
        ///<para>Example: n = 2 results in a 5x5 array kernel</para>
        /// </param>
        ///</summary>
        public static Mat LowPassAvgCV(Mat source, uint nKernel)
        {
            var filterResult = source.Clone();
            var kernelDimensions = 2 * nKernel + 1;
            double kernelValue = 1.0 / (kernelDimensions * kernelDimensions); //The sum of the values in the kernel must be equal to 1 to maintain same brightness
            var kernelDef = new double[kernelDimensions, kernelDimensions];
            for(int i = 0; i < kernelDimensions; i++)
            {
                for(int j = 0; j < kernelDimensions; j++)
                {
                    kernelDef[i,j] = kernelValue;
                }
            }
            var kernel = new Matrix<double>(kernelDef);

            CvInvoke.Filter2D(source, filterResult, kernel, new System.Drawing.Point(-1, -1));

            return filterResult;
        }

        public static Mat FilterCV(Mat source, FilterType filterType)
        {
            var filterResult = source.Clone();
            double[,] kernel;
            switch (filterType)
            {
                case FilterType.HomeMadeHP:
                    {
                        kernel = HomeMadeHP;
                        break;
                    }
                case FilterType.HomeMadeHP2:
                    {
                        kernel = HomeMadeHP2;
                        break;
                    }
                case FilterType.HomeMadeHP3:
                    {
                        kernel = HomeMadeHP3;
                        break;
                    }
                case FilterType.LaplaceGaussianHP:
                    {
                        kernel = LaplaceGaussianHP;
                        break;
                    }
                case FilterType.OddShaped1:
                    {
                        kernel = OddShaped1;
                        break;
                    }
                default:
                    {
                        kernel = LaplaceGaussianHP;
                        break;
                    }
            }
            //var sum = kernel.Cast<double>().Sum();
            //Console.WriteLine(String.Format("Kernel sum: {0}", sum));
            var kernelMatrix = new Matrix<double>(kernel);

            CvInvoke.Filter2D(source, filterResult, kernelMatrix, new System.Drawing.Point(-1, -1));

            return filterResult;
        }

        [Obsolete("Use LowpassCV() instead that is implemented with OpenCV's Filter2D that is much faster.")]
        public static Image<Gray, byte> LowpassAverage(Image<Gray, byte> original, uint n)
        {
            var result = original.Clone();

            for(int i = 0; i < original.Width; i ++)
            {
                for(int j = 0; j < original.Height; j++)
                {
                    var value = result[j, 1];
                    double quadraticAverage = GetQuadraticAverage(original, i, j, n);
                    value.MCvScalar = new MCvScalar(quadraticAverage);
                    result[j, i] = value;
                }
            }

            return result;
        }

        private static double GetQuadraticAverage(Image<Gray, byte> original, int i, int j, uint n)
        {
            double sum = 0;
            double count = 0;
            for (int k = i - (int)n; k <= i + n; k++)
            {
                for (int l = j - (int)n; l <= j + n; l++)
                {
                    var xCoordinate = k;
                    var yCoordinate = l;

                    //Keep coordinites within image matrix
                    if (xCoordinate < 0)
                    {
                        continue;
                    }
                    else if( xCoordinate >= original.Width)
                    {
                        continue;
                    }
                    else if(yCoordinate < 0)
                    {
                        continue;
                    }
                    else if(yCoordinate >= original.Height)
                    {
                        continue;
                    }
                    count++;
                    sum += original[yCoordinate, xCoordinate].Intensity;
                }
            }
            return sum/count;
        }

        ///<summary>
        ///<para>l</para>
        ///<param name="percentile">
        ///<para>The brightness percentile the center pixel must be equal to.</para>
        ///<para>Value between 0-1.</para>
        /// </param>
        ///</summary>
        public static Image<Gray, byte> FractileFilter(Image<Gray, byte> original, uint n, double percentile){

            var result = original.Clone();

            var filterArray = new int[2 * n + 1, 2 * n + 1];

            for (int i = 0; i < original.Width; i++)
            {
                for (int j = 0; j < original.Height; j++)
                {
                    var value = result[j, 1];
                    double[] windowPixelIntensities = GetWindowPixelIntensities(original, i, j, n);
                    Array.Sort(windowPixelIntensities);
                    uint percentileIndex = (uint)Math.Round(windowPixelIntensities.Length * percentile);
                    var percentileValue = windowPixelIntensities[percentileIndex];
                    value.MCvScalar = new MCvScalar(percentileValue);
                    result[j, i] = value;
                }
            }

            return result;
        }

        private static double[] GetWindowPixelIntensities(Image<Gray, byte> original, int i, int j, uint n)
        {
            var result = new List<double>();
            for (int k = i - (int)n; k <= i + n; k++)
            {
                for (int l = j - (int)n; l <= j + n; l++)
                {
                    var xCoordinate = k;
                    var yCoordinate = l;

                    //Keep coordinites within image matrix
                    if (xCoordinate < 0)
                    {
                        continue;
                    }
                    else if (xCoordinate >= original.Width)
                    {
                        continue;
                    }
                    else if (yCoordinate < 0)
                    {
                        continue;
                    }
                    else if (yCoordinate >= original.Height)
                    {
                        continue;
                    }
                    result.Add(original[yCoordinate, xCoordinate].Intensity);
                }
            }
            return result.ToArray();
        }
    }
}
