﻿using Emgu.CV;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarCounter
{
    public static class ImageExtensions
    {
        public static Image<TColor, TDepth> Rotate90<TColor, TDepth>(this Image<TColor, TDepth> img)
               where TColor : struct, IColor
               where TDepth : new()
        {
            var rot = new Image<TColor, TDepth>(img.Height, img.Width);
            CvInvoke.Transpose(img, rot);
            rot._Flip(Emgu.CV.CvEnum.FlipType.Horizontal);
            return rot;
        }

        public static Image<TColor, TDepth> Rotate180<TColor, TDepth>(this Image<TColor, TDepth> img)
            where TColor : struct, IColor
            where TDepth : new()
        {
            var rot = img.CopyBlank();
            rot = img.Flip(Emgu.CV.CvEnum.FlipType.Vertical);
            rot._Flip(Emgu.CV.CvEnum.FlipType.Horizontal);
            return rot;
        }

        public static void _Rotate180<TColor, TDepth>(this Image<TColor, TDepth> img)
            where TColor : struct, IColor
            where TDepth : new()
        {
            img._Flip(Emgu.CV.CvEnum.FlipType.Vertical);
            img._Flip(Emgu.CV.CvEnum.FlipType.Horizontal);
        }

        public static Image<TColor, TDepth> Rotate270<TColor, TDepth>(this Image<TColor, TDepth> img)
            where TColor : struct, IColor
            where TDepth : new()
        {
            var rot = new Image<TColor, TDepth>(img.Height, img.Width);
            CvInvoke.Transpose(img, rot);
            rot._Flip(Emgu.CV.CvEnum.FlipType.Vertical);
            return rot;
        }
    }
}
