﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarCounter
{
    public class GeometricUtils
    {
        public static double Distance(PointF point1, PointF point2)
        {
            return Math.Sqrt(Math.Pow(point1.X - point2.X, 2) + Math.Pow(point1.Y - point2.Y, 2));
        }

        public static double Distance(Point point1, Point point2)
        {
            return Math.Sqrt(Math.Pow(point1.X - point2.X, 2) + Math.Pow(point1.Y - point2.Y, 2));
        }

        internal static double CalcAngle(PointF center, PointF current, PointF previous)
        {
            var p1 = PointF.Subtract(previous, new SizeF(center));
            var p2 = PointF.Subtract(current, new SizeF(center));
            var cosTheta = (p1.X * p2.X + p1.Y * p2.Y) / (Math.Sqrt(p1.X * p1.X + p1.Y * p1.Y) * Math.Sqrt(p2.X * p2.X + p2.Y * p2.Y));
            var sinTheta = (p1.X * p2.Y - p2.X * p1.Y) / (Math.Sqrt(p1.X * p1.X + p1.Y * p1.Y) * Math.Sqrt(p2.X * p2.X + p2.Y * p2.Y));
            return Math.Atan2(sinTheta, cosTheta);
            //return Math.Acos((p1.X*p2.X + p1.Y * p2.Y) / (Math.Sqrt(p1.X*p1.X + p1.Y*p1.Y)*Math.Sqrt(p2.X*p2.X + p2.Y*p2.Y)));
        }

        public static PointF RotatePoint(PointF point, double rad)
        {
            var result = new PointF();
            result.X = (float)(point.X * Math.Cos(rad) - point.Y * Math.Sin(rad));
            result.Y = (float)(point.X * Math.Sin(rad) + point.Y * Math.Cos(rad));
            return result;
        }

        public static float Rad2Deg(float rad)
        {
            return rad / (float)(Math.PI) * 180f;
        }

        public static float Deg2Rad(float deg)
        {
            return deg / 180f * (float)Math.PI;
        }


        public static double Rad2Deg(double rad)
        {
            return rad / (Math.PI) * 180.0;
        }

    }
}
