﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarCounter
{
    public class FilterDefinitions
    {
        public enum FilterType
        {
            LaplaceGaussianHP = 1,
            HomeMadeHP = 100,
            HomeMadeHP2 = 101,
            HomeMadeHP3 = 102,
            OddShaped1 = 200,
        }

        #region Filter definitions
        public readonly static double[,] HomeMadeHP = new double[5, 5] {    
            {0.5, 0.5, 0.5, 0.5, 0.5},
            {0.5, -0.5, -0.5, -0.5, 0.5},
            {0.5, -0.5, -4.0, -0.5, 0.5},
            {0.5, -0.5, -0.5, -0.5, 0.5},
            {0.5, 0.5, 0.5, 0.5, 0.5}
        };

        public readonly static double[,] HomeMadeHP2 = new double[5, 5] {   
            {0.25, 0.25, 0.25, 0.25, 0.25},
            {0.25, 0.5, 0.5, 0.5, 0.25},
            {0.25, 0.5, -8.0, 0.5, 0.25},
            {0.25, 0.5, 0.5, 0.5, 0.25},
            {0.25, 0.25, 0.25, 0.25, 0.25}
        };

        public readonly static double[,] HomeMadeHP3 = new double[7, 7] {   
            {0.00, 0.50, 0.50, 1.00, 0.50, 0.50, 0.00},
            {0.50, 1.50, 1.50, 1.50, 1.50, 1.50, 0.50},
            {0.50, 1.50, -1.5, -1.5, -1.5, 1.50, 0.50},
            {1.00, 1.50, -1.5, -24, -1.5, 1.50, 1.00},
            {0.50, 1.50, -1.5, -1.5, -1.5, 1.50, 0.50},
            {0.50, 1.50, 1.50, 1.50, 1.50, 1.50, 0.50},
            {0.00, 0.50, 0.50, 1.00, 0.50, 0.50, 0.00}
        };

        public readonly static double[,] LaplaceGaussianHP = new double[9, 9] { 
            {0, 0, 1, 2, 2, 2, 1, 0, 0},
            {0, 1, 5, 10, 12, 10, 5, 1, 0},
            {1, 5, 15, 19, 16, 19, 15, 5, 1},
            {2, 10, 19, -19, -64, -19, 19, 10, 2},
            {2, 12, 16, -64, -148, -64, 16, 12, 2},
            {2, 10, 19, -19, -64, -19, 19, 10, 2},
            {1, 5, 15, 19, 16, 19, 15, 5, 1},
            {0, 1, 5, 10, 12, 10, 5, 1, 0},
            {0, 0, 1, 2, 2, 2, 1, 0, 0}
        };

        public static readonly double[,] OddShaped1 = new double [5, 5]
        {
            {0.00, 0.00, 6.00, 0.00, 0.00},
            {0.00, 0.00, 3.00, 0.00, 0.00},
            {0.00, 2.00, 2.00, 2.00, 0.00},
            {1.00, 1.00, 1.00, 1.00, 1.00},
            {-4.0, -4.0, -4.0, -4.0, -4.0}
        };

        #endregion
    }
}
