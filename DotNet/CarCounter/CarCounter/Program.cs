﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Emgu.CV;
using Emgu.CV.UI;
using Emgu.CV.Structure;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.Diagnostics;
using Emgu.CV.Cuda;
using MathNet.Numerics;
using Emgu.CV.Features2D;
using MathNet.Numerics.LinearAlgebra;
using Emgu.CV.Util;
using CarCounter.DataClasses;

namespace CarCounter
{
    class Program
    {
        public const string EXIT_COMMAND = "exit";

        private enum InputCommand
        {
            Exit = 0,
            OutputCroppedImages = 1,
            OutputCroppedTestImages = 2,
            DrawLocations = 3,
            CreateTrainSetNumpy = 4,
            CreateTestSetNumpy = 5,
            CreateSegmentsFromImage = 6,
            DrawPredictionResults = 7,
            CreateRoadSegmentsFromFolder = 8,
            CreateTreeSegmentsFromFolder = 9,
            CreateGrassSegmentsFromFolder = 10,
            CreateAllTrainingAndTestingData = 11,
            CreateAllTrainingData = 12,
            CropTestImageWithAnnotations = 13,
            CropTrainingImageWithAnnotations = 14,
            DrawTrainingPredictionResults = 15,
            DrawTrainingPredictionResultsOutput = 16,
            TestNonMaxCalculationTime = 17,
        }
        private const string FALSE_POSITIVES_OUTPUTPATH = "C:\\CarCounterData\\CowcData\\Training\\FalsePositives";

        //private const string PREDICTION_BASE_PATH = "C:\\CarCounterData\\CowcData\\Training\\Modified\\";
        private const string PREDICTION_BASE_PATH = "C:\\CarCounterData\\CowcData\\Testing\\Modified\\";
        //private const string PREDICTION_IMAGE_FILENAME = "12TVL160660-CROP_road_section.png";
        //private const string PREDICTION_IMAGE_FILENAME = "12TVL160640-CROP_cropped.png";
        //private const string PREDICTION_IMAGE_FILENAME = "12TVL240120_parking_lots.png"; //Quite large image approx 3k x 3k
        private const string PREDICTION_IMAGE_FILENAME = "12TVL240120(3532,9400)(5334,11245).png"; // TESTING Has annotations
        //private const string PREDICTION_IMAGE_FILENAME = "12TVL160640-CROP(5200,3850)(6200,4850).png"; // TESTING Has annotations
        //private const string PREDICTION_IMAGE_FILENAME = "03747(7532,1288)(9532,3288).png"; // TRAINING Has annotations

        private static string PredictionSegmentsPath
        {
            get => Path.Combine(PREDICTION_BASE_PATH, $"Segments{CowcHelper.CarPixelSize}x{CowcHelper.CarPixelSize}_" + PREDICTION_IMAGE_FILENAME.Substring(0, PREDICTION_IMAGE_FILENAME.LastIndexOf('.')));
        }
        private static string PredictionImagePath
        {
            get => Path.Combine(PREDICTION_BASE_PATH, PREDICTION_IMAGE_FILENAME);
        }

        private static string PredictionImageAnnotationsPath
        {
            get => Path.Combine(PREDICTION_BASE_PATH, Path.GetFileNameWithoutExtension(PREDICTION_IMAGE_FILENAME) + "_Annotated_Cars.png");
        }

        static void Main(string[] args)
        {
            //prevent the JIT Compiler from optimizing Fkt calls away
            long seed = Environment.TickCount;

            //prevent "Normal" Processes from interrupting Threads
            Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.High;

            //prevent "Normal" Threads from interrupting this thread
            Thread.CurrentThread.Priority = ThreadPriority.Highest;

            InputCommand inputCommand;
            TryReadInputCommand(out inputCommand);

            switch (inputCommand)
            {
                case InputCommand.Exit:
                    return;
                case InputCommand.OutputCroppedImages:
                    new CowcHelper(isTestData: false).OutputCroppedImages(useRandomDistortions: true, onlyPositives: false);
                    break;
                case InputCommand.OutputCroppedTestImages:
                    new CowcHelper(isTestData: true).OutputCroppedImages(useRandomDistortions: false, onlyPositives: false);
                    break;
                case InputCommand.DrawLocations:
                    CropCarsFromImage();
                    break;
                case InputCommand.CreateTrainSetNumpy:
                    new CowcHelper(isTestData: false).CreateDataSetNumpy(onlyPositives: true);
                    break;
                case InputCommand.CreateTestSetNumpy:
                    new CowcHelper(isTestData: true).CreateDataSetNumpy(onlyPositives: true);
                    break;
                case InputCommand.CreateSegmentsFromImage:
                    CreateSegmentsFromImage();
                    break;
                case InputCommand.DrawPredictionResults:
                    DrawPredicionResults(CowcHelper.PredictionCategory.CarPositive);
                    break;
                case InputCommand.CreateRoadSegmentsFromFolder:
                    CreateRoadSegments();
                    break;
                case InputCommand.CreateTreeSegmentsFromFolder:
                    CreateTreeSegments();
                    break;
                case InputCommand.CreateGrassSegmentsFromFolder:
                    CreateGrassSegments();
                    break;
                case InputCommand.CreateAllTrainingAndTestingData:
                    CreateAllTrainingAndTestingData();
                    break;
                case InputCommand.CreateAllTrainingData:
                    CreateAllTrainingData();
                    break;
                case InputCommand.CropTestImageWithAnnotations:
                    CropImageWithAnnotations(isTestData: true);
                    break;
                case InputCommand.CropTrainingImageWithAnnotations:
                    CropImageWithAnnotations(isTestData: false);
                    break;
                case InputCommand.DrawTrainingPredictionResults:
                    DrawPredicionResults(CowcHelper.PredictionCategory.CarPositive, useColorGradients:true, outputTrainingData:false);
                    break;
                case InputCommand.DrawTrainingPredictionResultsOutput:
                    DrawPredicionResults(CowcHelper.PredictionCategory.CarPositive, useColorGradients: true, outputTrainingData: true);
                    break;
                case InputCommand.TestNonMaxCalculationTime:
                    TestNonMaxCalculationTime();
                    break;
                default:
                    return;
            }
        }

        private static void CropImageWithAnnotations(bool isTestData)
        {
            string imageNameWithoutExtension;
            Point upperLeft;
            Point bottomRight;

            //Test data
            if (isTestData)
            {
                //imageNameWithoutExtension = "12TVL240120";
                //upperLeft = new Point(3532, 9400);
                //bottomRight = new Point(5334, 11245);
                imageNameWithoutExtension = "12TVL160640-CROP";
                upperLeft = new Point(5200, 3850);
                bottomRight = new Point(6200, 4850);
            }
            //Training data
            else
            {
                imageNameWithoutExtension = "03747";
                upperLeft = new Point(7532, 1288);
                bottomRight = new Point(9532, 3288);
            }
           
            var boundingBox = new Rectangle(upperLeft, new Size(Point.Subtract(bottomRight, new Size(upperLeft))));

            var cowcHelper = new CowcHelper(isTestData);

            if (!cowcHelper.OriginalsByFilename.TryGetValue(imageNameWithoutExtension, out string imagePath))
                throw new Exception("Filename does not exist in originals dictionary");

            var suffix = $"({upperLeft.X},{upperLeft.Y})({bottomRight.X},{bottomRight.Y})";
            using (var source = new Image<Rgb, float>(imagePath))
            {
                using (var subrect = source.GetSubRect(boundingBox))
                {
                    subrect.Save(Path.Combine(PREDICTION_BASE_PATH, imageNameWithoutExtension + suffix + ".png"));
                }
            }

            if (!cowcHelper.AnnoCarsByFilename.TryGetValue(imageNameWithoutExtension, out imagePath))
                throw new Exception("Filename does not exist in annotated cars dictionary");

            using (var source = new Image<Rgb, float>(imagePath))
            {
                using (var subrect = source.GetSubRect(boundingBox))
                {
                    subrect.Save(Path.Combine(PREDICTION_BASE_PATH, imageNameWithoutExtension + suffix + "_Annotated_Cars.png"));
                }
            }
        }

        private static void CreateAllTrainingData() {
            new CowcHelper(isTestData: false).OutputCroppedImages(useRandomDistortions: true, onlyPositives: false);
            Console.WriteLine("Car training data complete");
            CreateRoadSegments();
            Console.WriteLine("Road training data complete");
            CreateTreeSegments();
            Console.WriteLine("Tree training data complete");
            CreateGrassSegments();
            Console.WriteLine("Grass training data complete");
        }

        private static void CreateAllTrainingAndTestingData()
        {
            new CowcHelper(isTestData: false).OutputCroppedImages(useRandomDistortions:true, onlyPositives: false);
            Console.WriteLine("Training data complete");
            new CowcHelper(isTestData: true).OutputCroppedImages(useRandomDistortions:false, onlyPositives: false);
            Console.WriteLine("Test data complete");
            CreateRoadSegments();
            Console.WriteLine("Road segments complete");
            CreateTreeSegments();
            Console.WriteLine("Tree segments complete");
            CreateGrassSegments();
            Console.WriteLine("Grass segments complete");
            CreateSegmentsFromImage();
            Console.WriteLine("All complete");
        }

        private static void CreateGrassSegments()
        {
            CreateSegmentsForTraining("C:\\CarCounterData\\CowcData\\Training\\Grass");
        }

        private static void CreateTreeSegments()
        {
            CreateSegmentsForTraining("C:\\CarCounterData\\CowcData\\Training\\Trees");
        }

        private static void CreateRoadSegments()
        {
            //CreateSegmentsForTraining("C:\\CarCounterData\\CowcData\\Training\\Roads", rotate:true, brightnessDistSpan: new IntSpan(-125, 15), contrastDistPercentage: new IntSpan(75, 110));
            CreateSegmentsForTraining("C:\\CarCounterData\\CowcData\\Training\\Roads");
        }

        private static void TestNonMaxCalculationTime()
        {
            var dict = CowcHelper.ParsePointDictionary(Path.Combine(PredictionSegmentsPath, "dictionary_int_Point.txt"));
            var predictions = CowcHelper.ParsePredictions(Path.Combine(PredictionSegmentsPath, "predictions.txt"));
            var size = new Size(CowcHelper.CarPixelSize, CowcHelper.CarPixelSize);
            var minProb = 0;
            var bestPredictions = new List<PredictionSegment>();
            for (int i = 0; i < predictions.Count(); i++)
            {
                var probability = predictions[i].Item2;
                if (predictions[i].Item1 == (int)CowcHelper.PredictionCategory.CarPositive && probability > minProb)
                {
                    var pos = dict[i + 1]; //ID's start from 1
                    bestPredictions.Add(new PredictionSegment(probability, new Rectangle(pos, size)));
                }
            }

            bestPredictions.Shuffle();
            var maxPredictions = 100000;
            bestPredictions = bestPredictions.TakeWhile((x,i) => i < maxPredictions).ToList();

            var prioritizeBest = false;
            var overlap = 0.17;

            var sw = new Stopwatch();
            var iterationSize = 500;
            var results = new List<Tuple<int, long>>();
            var outputPath = Path.Combine(PREDICTION_BASE_PATH, Path.GetFileNameWithoutExtension(PREDICTION_IMAGE_FILENAME) + $"_{DateTime.Now.ToString("HH.mm.ss")}.txt");
            using (StreamWriter writer = File.AppendText(outputPath))
            {
                for (int i = iterationSize; i < bestPredictions.Count; i += iterationSize)
                {
                    try
                    {
                        var testSet = bestPredictions.Take(i);
                    }
                    catch
                    {
                        break;
                    }
                
                    sw.Restart();

                    var filteredBBs = ImageUtils.NonMaximumSuppression(bestPredictions, overlap, prioritizeBest);

                    var elapsedMs = sw.ElapsedMilliseconds;
                    results.Add(new Tuple<int, long>(i, elapsedMs));
                    writer.WriteLine($"{i}\t{elapsedMs}");
                }
            }
        }

        private static void DrawPredicionResults(CowcHelper.PredictionCategory category = CowcHelper.PredictionCategory.CarPositive, bool useColorGradients = true, bool outputTrainingData = false)
        {
            var sw = new Stopwatch();
            sw.Start();
           
            var dict = CowcHelper.ParsePointDictionary(Path.Combine(PredictionSegmentsPath, "dictionary_int_Point.txt"));
            var predictions = CowcHelper.ParsePredictions(Path.Combine(PredictionSegmentsPath, "predictions.txt"));
            var size = new Size(CowcHelper.CarPixelSize, CowcHelper.CarPixelSize);
            var minProb = 0.9;
            var bestPredictions = new List<PredictionSegment>();
            for (int i = 0; i < predictions.Count(); i++)
            {
                var probability = predictions[i].Item2;
                //TESTING
                //category = CowcHelper.PredictionCategory.CarNegative;
                if (predictions[i].Item1 == (int)category && probability > minProb)
                {
                    var pos = dict[i + 1]; //ID's start from 1
                    bestPredictions.Add(new PredictionSegment(probability, new Rectangle(pos, size)));
                }
            }
            var prioritizeBest = true;
            var overlap = 0.17;

            var parseTime = sw.Elapsed.ToString($"mm\\:ss\\.fff");
            sw.Restart();
            //var filteredBBs = ImageUtils.NonMaximumSuppression(bestPredictions, overlap, prioritizeBest);
            var filteredBBs = ImageUtils.NonMaximumSuppression(bestPredictions, overlap, false);
            //var filteredBBs = bestPredictions;

            //Compare with ground truths if possible
            List<Point> groundTruths = new List<Point>();
            if (File.Exists(PredictionImageAnnotationsPath))
            {
                groundTruths = CowcHelper.ParseCarAnnotation(PredictionImageAnnotationsPath);
            }
            else
            {
                Console.WriteLine("No annotations found.");
            }
            var truePositives = 0;
            var falseNegatives = 0;
            foreach(var gt in groundTruths)
            {
                var results = filteredBBs.Where(x => x.BoundingBox.Contains(gt)).ToList();
                if (!results.Any())
                {
                    falseNegatives++;
                }
                else if(results.Count > 1)
                {
                    var gtF = (PointF)gt;
                    var shortestEuclidianDistance = Double.MaxValue;
                    PredictionSegment best = null;
                    foreach (var bb in results)
                    {
                        float yCenter = bb.BoundingBox.Y + bb.BoundingBox.Height / 2.0f;
                        float xCenter = bb.BoundingBox.X + bb.BoundingBox.Width / 2.0f;
                        var dist = GeometricUtils.Distance(gtF, new PointF(xCenter, yCenter));
                        if(dist < shortestEuclidianDistance)
                        {
                            shortestEuclidianDistance = dist;
                            best = bb;
                        }
                        bb.TruePositive = false;
                    }
                    best.TruePositive = true;
                }
                else
                {
                    results.First().TruePositive = true;
                    truePositives++;
                }
            }
            var falsePositives = filteredBBs.Count(x => x.TruePositive == null);

            if (outputTrainingData)
            {
                ExportFalsePositives(filteredBBs.Where(x => x.TruePositive == null).ToList(), groundTruths);
            }
            var precision = 1.0* truePositives / (truePositives + falsePositives);
            var recall = 1.0 * truePositives / groundTruths.Count();
            var f1Score = 2.0 * (precision * recall) / (precision + recall);
            var calcTime = sw.Elapsed.ToString($"mm\\:ss\\.fff");

            using (var source = new Image<Rgb, byte>(PredictionImagePath))
            {
                DrawingUtils.DrawHeaderText($"Det: {filteredBBs.Count()}, f1:{f1Score:0.000}, TP: {truePositives}, FP: {falsePositives}, FN: {falseNegatives}, calc: {calcTime}, parse: {parseTime}", source, 1.2);

                if (useColorGradients) {
                    var gradientBins = DrawingUtils.GetColorGradientBins(Color.Red, Color.FromArgb(255, 0, 255, 0), (int)((1 - minProb) * 100) + 1); //Bin per percentage
                    foreach (var bb in filteredBBs)
                    {
                        var binIndex = (int)(Math.Ceiling((bb.Probability - minProb) * 100)) - 1;
                        source.Draw(bb.BoundingBox, new Rgb(gradientBins[binIndex]));
                    }
                }
                else{
                    foreach (var bb in filteredBBs)
                    {
                        source.Draw(bb.BoundingBox, new Rgb(Color.Green));
                    }
                }
                
                var fileName = Path.GetFileNameWithoutExtension(PredictionImagePath);
                source.Save(Path.Combine(PREDICTION_BASE_PATH, $"{fileName}_pred_minP-{minProb:0.00}_pb-{prioritizeBest}_ol-{overlap:0.00}_{category}.png"));
            }
        }

        private static void ExportFalsePositives(List<PredictionSegment> falsePositives, List<Point> groundTruths, float distanceThresholdMeters = 5)
        {
            if (!PredictionImagePath.Contains("\\Training\\"))
                throw new Exception("Image should be from training set!");

            var size = falsePositives.First().BoundingBox.Width;
            var outputPath = FALSE_POSITIVES_OUTPUTPATH + $"{size}x{size}";
            if (!Directory.Exists(outputPath))
            {
                Directory.CreateDirectory(outputPath);
            }

            var minDistancePixels = (int)Math.Round(distanceThresholdMeters / CowcHelper.GSD);
            var centerOffset = (int)Math.Round(size / 2.0) - minDistancePixels;
            var surroundingSize = 2 * minDistancePixels;

            var certainTruePositives = new List<Tuple<PredictionSegment, Point>>();
            foreach (var fp in falsePositives)
            {
                var x = fp.BoundingBox.X + centerOffset;
                var y = fp.BoundingBox.Y + centerOffset;
                var centerPoint = new Point(x + minDistancePixels, y + minDistancePixels);
                var surroundingBB = new Rectangle(x, y, surroundingSize, surroundingSize);
                var tooCloseToCar = groundTruths.Where(gt => surroundingBB.Contains(gt)).Any(gt => GeometricUtils.Distance(gt, centerPoint) < minDistancePixels);
                if (!tooCloseToCar)
                {
                    certainTruePositives.Add(new Tuple<PredictionSegment, Point>(fp, centerPoint));
                }
            }
            var source = new Image<Rgb, float>(PredictionImagePath);
            var sourceFilename = Path.GetFileNameWithoutExtension(PredictionImagePath);
            foreach (var fp in certainTruePositives)
            {
                using (var subrect = source.GetSubRect(fp.Item1.BoundingBox))
                {
                    var filename = sourceFilename + $"_({fp.Item2.X}, {fp.Item2.Y})_" + Guid.NewGuid().ToString() + ".png";
                    subrect.Save(Path.Combine(outputPath, filename));
                }
            }
        }

        private static void CreateSegmentsFromImage()
        {
            var source = new Image<Rgb, float>(PredictionImagePath);
            IOTools.CleanOrCreateDirectory(PredictionSegmentsPath);
            CowcHelper.SplitIntoSegments(source, new Size(CowcHelper.CarPixelSize, CowcHelper.CarPixelSize), PredictionSegmentsPath, "", 3, 3, createDictionary:true);
        }

        private static void CreateSegmentsForTraining(string basePath, bool rotate = true, IntSpan brightnessDistSpan = null , IntSpan contrastDistPercentage = null)
        {
            var files = Directory.GetFiles(basePath).Where(x => Path.GetExtension(x).ToLower() == ".png");
            var outputPath = Path.Combine(basePath, $"Segments{CowcHelper.CarPixelSize}x{CowcHelper.CarPixelSize}");
            IOTools.CleanOrCreateDirectory(outputPath);
            foreach (var file in files)
            {
                var source = new Image<Rgb, float>(file);
                var filename = Path.GetFileNameWithoutExtension(file);
                CowcHelper.SplitIntoSegments(source, new Size(CowcHelper.CarPixelSize, CowcHelper.CarPixelSize), outputPath, filename + "0deg", skippingHorizontal: null, skippingVertical: null, createDictionary: false, useRandomDistortions: true,  brightnessDistSpan:brightnessDistSpan, contrastSpanPercentage: contrastDistPercentage);
                if (rotate)
                {
                    source = ImageExtensions.Rotate90(source);
                    CowcHelper.SplitIntoSegments(source, new Size(CowcHelper.CarPixelSize, CowcHelper.CarPixelSize), outputPath, filename + "90deg", skippingHorizontal: null, skippingVertical: null, createDictionary: false, useRandomDistortions: true, brightnessDistSpan: brightnessDistSpan, contrastSpanPercentage: contrastDistPercentage);
                    source = ImageExtensions.Rotate90(source);
                    CowcHelper.SplitIntoSegments(source, new Size(CowcHelper.CarPixelSize, CowcHelper.CarPixelSize), outputPath, filename + "180deg", skippingHorizontal: null, skippingVertical: null, createDictionary: false, useRandomDistortions: true, brightnessDistSpan: brightnessDistSpan, contrastSpanPercentage: contrastDistPercentage);
                    source = ImageExtensions.Rotate90(source);
                    CowcHelper.SplitIntoSegments(source, new Size(CowcHelper.CarPixelSize, CowcHelper.CarPixelSize), outputPath, filename + "270deg", skippingHorizontal: null, skippingVertical: null, createDictionary: false, useRandomDistortions: true, brightnessDistSpan: brightnessDistSpan, contrastSpanPercentage: contrastDistPercentage);
                }
            }
        }

        private static void CropCarsFromImage()
        {
            var viewer = new ImageViewer();
            viewer.ImageBox.SizeMode = PictureBoxSizeMode.AutoSize;
            viewer.AutoSize = true;

            viewer.Image = CowcHelper.GetImageWithCarPos();

            viewer.ShowDialog();
        }

        private static void TryReadInputCommand(out InputCommand input)
        {
            var inputChoices = String.Concat(EnumUtil.GetValues<InputCommand>().Select(x => $"[{(int)x}] {x}\n").ToArray());

            Console.WriteLine($"You have the following options:\n{inputChoices}");
            string inputString = Console.ReadLine();
            inputString = inputString.Trim();
            int inputInt;
            if (int.TryParse(inputString, out inputInt) && Enum.IsDefined(typeof(InputCommand), inputInt))
            {
                input = (InputCommand)inputInt;   
            }
            else
            {
                Console.WriteLine("Incorrect input. Try again:");
                TryReadInputCommand(out input);
            }
        }
    }
}
