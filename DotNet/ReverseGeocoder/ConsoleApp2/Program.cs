﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Emgu.CV;
using Emgu.Util;
using Emgu.Util.TypeEnum;

namespace ConsoleApp2
{
    class Program
    {

        private static List<string> listStreets = new List<string>();
        private static List<double[]> listDegs = new List<double[]>();
        static void Main(string[] args)
        {

            //Given values for car localization sample.
            float GSD = 12.5f;
            double image_Lat = 55.672079;
            double image_Long = 12.570484;

            //float car_x = 1573;
            //float car_y = 1028;


            //float car_dist_x = car_x * GSD / 100;
            //float car_dist_y = car_y * GSD / 100;

            //double car_lat = image_Lat - (car_dist_y / 111319.9);
            //double car_long = image_Long + (car_dist_x / (40075161.2 * Math.Cos(image_Lat) / 360));

            //Debug.WriteLine(car_lat);
            //Debug.WriteLine(car_long);
            //Begin extracting info from localized database.

            var reader = new StreamReader(@"C:\Test\newblopFile.csv");

            bool skipFirst = false;

            while (!reader.EndOfStream)
            {

                var s = reader.ReadLine();
                if (!skipFirst)
                {
                    skipFirst = true;
                    continue;
                }
                listStreets.Add(s.Split(',')[3]);
                listDegs.Add(new double[] {
                    double.Parse(s.Split(',')[1], System.Globalization.CultureInfo.InvariantCulture),
                    double.Parse(s.Split(',')[2], System.Globalization.CultureInfo.InvariantCulture)
                });

            }

            //Sample coordinates in image south east of Tivoli
            List<double[]> listCars = new List<double[]>();

            listCars.Add(new double[] { 80, 301 }); // Stoltenbergsgade
            listCars.Add(new double[] { 130, 43 }); // Ved Glyptoteket
            listCars.Add(new double[] { 599, 309 }); // Ved Glyptoteket
            listCars.Add(new double[] { 222, 701 }); // Edvard Falcks Gade
            listCars.Add(new double[] { 993, 635 }); // Otto Mønsteds Gade
            listCars.Add(new double[] { 1313, 403 }); // Anker Heegaards Gade
            listCars.Add(new double[] { 850, 248 }); // Niels Brocks Gade

            //Check console for street names to compare with those listed above (Actual street names).
            foreach (double[] car in listCars)
            {
                Debug.WriteLine(getStreetFromPixels(image_Lat, image_Long, GSD, car[0], car[1]));
            }

            //Debug.WriteLine(listStreets[1]);
            //Debug.WriteLine(listDegs[1][0] + " " + listDegs[1][1]);

            //for(int i = 0; i < 1000; i++)
           // Debug.WriteLine(getStreet(car_lat, car_long));

            //RetrieveFormatedAddress(image_Lat.ToString(CultureInfo.InvariantCulture), image_Long.ToString(CultureInfo.InvariantCulture));
            //Console.ReadKey();
            //Console.ReadKey();
        }

        public static string getStreetFromPixels(double image_Lat, double image_Long, double GSD,double car_x, double car_y)
        {
            double car_dist_x = car_x * GSD / 100;
            double car_dist_y = car_y * GSD / 100;

            double car_lat = image_Lat - (car_dist_y / 111319.9);
            double car_long = image_Long + (car_dist_x / (40075161.2 * Math.Cos(image_Lat) / 360));

            return getStreet(car_lat, car_long);
        }

        public static string getStreet(double carLat, double carLong)
        {
            double shortestDis = Math.Sqrt(Math.Pow(carLong - listDegs[0][0], 2) + Math.Pow(carLat - listDegs[0][1], 2));
            int shortestIndex = 0;
            for(int i = 1; i < listDegs.Count(); i++)
            {
                double curDis = Math.Sqrt(Math.Pow(carLong - listDegs[i][0], 2) + Math.Pow(carLat - listDegs[i][1], 2));
                if (curDis < shortestDis)
                {
                    shortestDis = curDis;
                    shortestIndex = i;
                }
            }
            return listStreets[shortestIndex];

        }
        //static string baseUri = "http://maps.googleapis.com/maps/api/geocode/xml?latlng={0},{1}&ey=AIzaSyBZc9_5ljk55L4tVXSJmmhYOl4_LBY13o0";
        //string location = string.Empty;

        //public static void RetrieveFormatedAddress(string lat, string lng)
        //{
        //    string requestUri = string.Format(baseUri, lat, lng);
        //    //Debug.Write(requestUri);
        //    //requestUri =  "http://maps.googleapis.com/maps/api/geocode/xml?latlng=55.6730228631715,12.5663530826569&ey=AIzaSyBZc9_5ljk55L4tVXSJmmhYOl4_LBY13o0";
        //    var wc = new WebClient();
        //    wc.DownloadStringCompleted += (sender, args) =>
        //    {
        //        var xmlElm = XElement.Parse(args.Result);
        //        var status = (from elm in xmlElm.Descendants()
        //            where
        //                elm.Name == "status"
        //            select elm).FirstOrDefault();
        //        if (status.Value.ToLower() == "ok")
        //        {
        //            var res = (from elm in xmlElm.Descendants()
        //                where
        //                    elm.Name == "long_name"
        //                select elm).ElementAtOrDefault(1);
        //            requestUri = res.Value;
        //            Debug.WriteLine(res.Value);
        //        }
        //    };
        //    wc.DownloadStringAsync(new Uri(requestUri));
        //    return;
            //using (WebClient wc = new WebClient())
            //{
            //    string result = wc.DownloadString(requestUri);
            //    var xmlElm = XElement.Parse(result);
            //    var status = (from elm in xmlElm.Descendants()
            //        where
            //            elm.Name == "status"
            //        select elm).FirstOrDefault();
            //    if (status.Value.ToLower() == "ok")
            //    {
            //        var res = (from elm in xmlElm.Descendants()
            //            where
            //                elm.Name == "long_name"
            //            select elm).ElementAtOrDefault(1);
            //        requestUri = res.Value;
            //        Debug.WriteLine(res.Value);
            //    }
            //}
            ////Debug.WriteLine(requestUri);
        //}
    }
}
