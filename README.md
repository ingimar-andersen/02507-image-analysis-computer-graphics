# README #

### What is this repository for? ###
Car detection from overhead imagery using DNN's implemented using Tensorflow as a part of the course [Project in Image Analysis and Computer Graphics](http://kurser.dtu.dk/course/02507) at DTU.
The training and prediction are implemented in Python while analysis and visual representation is implemented in C#.

### How do I get set up? ###
For using the CarCounter project EmguCV v.3.3 must be installed.
The Python part requires Python 3.3+ and Tensorflow. Installation guide for Tensorflow: https://www.tensorflow.org/versions/r0.12/get_started/os_setup

The required Python dependencies can be installed using:
pip install -r requirements.txt
