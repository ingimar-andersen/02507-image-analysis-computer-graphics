import tensorflow as tf
import numpy as np
import matplotlib.image as mpimg
import time
import os
import contextlib
from os import listdir
from os.path import isfile, join

import CarsDnnClassifier
import constants as cn
import input_handler as input

def main(argv=None):
    base_dir = "/CarCounterData/CowcData/Testing/Modified/"
    # base_dir = "/CarCounterData/CowcData/Training/Modified/"
    prefix = "Segments" + str(cn.IMAGE_SIZE) + "x" + str(cn.IMAGE_SIZE)

    # file_name = prefix + "_12TVL160640-CROP_cropped"
    # file_name = prefix + "_12TVL160660-CROP_road_section"
    # file_name = prefix + "_12TVL240120_parking_lots"
    #file_name = prefix + "_12TVL240120(3532,9400)(5334,11245)"
    file_name = prefix + "_12TVL160640-CROP(5200,3850)(6200,4850)"
    # file_name = prefix + "_03747(7532,1288)(9532,3288)"
    data_dir = os.path.join(base_dir, file_name)
    # images_test, labels_test = CarsDnnClassifier.read_cars_input(data_dir, isTest=True, takeFirst=None, onlyInclude=True)
    images_test = input.read_input(data_dir)

    classifier = CarsDnnClassifier.get_classifier()

    # Predict using categories using new data
    predict_input_fn = tf.estimator.inputs.numpy_input_fn(
        x={"x": images_test},
        num_epochs=1,
        shuffle=False)

    predictions = list(classifier.predict(input_fn=predict_input_fn))
    predicted_classes = [[p["classes"], p["probabilities"].max()] for p in predictions]

    prediction_filepath = os.path.join(data_dir, "predictions.txt")
    with contextlib.suppress(FileNotFoundError):
        os.remove(prediction_filepath)
    with open(prediction_filepath, 'a') as file:
        for p in predicted_classes:
            cat = int(p[0][0])
            prob = p[1]
            file.write("{0:d} {1:f}\n".format(cat, prob))

if __name__ == '__main__':
    tf.app.run()