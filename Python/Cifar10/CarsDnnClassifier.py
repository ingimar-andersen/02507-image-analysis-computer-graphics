#Adapted from: https://codeburst.io/use-tensorflow-dnnclassifier-estimator-to-classify-mnist-dataset-a7222bf9f940

import tensorflow as tf
import numpy as np
import matplotlib.image as mpimg
import time
import math

import os
from os import listdir
from os.path import isfile, join

import constants as cn
import input_handler as input

from tensorflow.examples.tutorials.mnist import input_data

import CarsDnnClassifier_Eval

def get_classifier(numClasses=cn.NUM_CLASSES):
    # Specify feature
    feature_columns = [tf.feature_column.numeric_column("x", shape=[cn.IMAGE_SIZE, cn.IMAGE_SIZE, cn.IMAGE_BANDS])]
    model_dir = "./tmp/cars_model" + str(cn.IMAGE_SIZE)
    # Build 2 layer DNN classifier

    return tf.estimator.DNNClassifier(
        feature_columns=feature_columns,
        hidden_units=[2048, 512, 64],
        optimizer=tf.train.AdamOptimizer(learning_rate=1e-4, beta1=0.9, beta2=0.999, epsilon=1e-8),
        n_classes=numClasses,
        dropout=0.2,  # 0.1
        model_dir=model_dir
    )


def main(argv:None):
    data_dir = "/CarCounterData/CowcData/"

    onlyFirstTwo = True

    if onlyFirstTwo:
        images_train, labels_train = input.read_cars_input(data_dir, isTest=False, takeFirst=None, onlyInclude=None)
        numClasses = 2
    else:
        images_train1, labels_train1 = input.read_cars_input(data_dir, isTest=False, takeFirst=None, onlyInclude=None)
        backgrounds_dir = "/CarCounterData/CowcData/Training/"
        images_train2, labels_train2 = input.read_backgrounds_input(backgrounds_dir, takeFirst=None, discardNegatives=False)
        images_train = np.concatenate([images_train1, images_train2], 0)
        labels_train = np.concatenate([labels_train1, labels_train2], 0)
        numClasses = cn.NUM_CLASSES

    classifier = get_classifier(numClasses=numClasses)

    # Define the training inputs
    batch_size = 128
    train_input_fn = tf.estimator.inputs.numpy_input_fn(
        x={"x": images_train},
        y=labels_train,
        num_epochs=None,
        batch_size=batch_size,
        shuffle=True,
        queue_capacity=5000,
        num_threads = 8,
    )
    start = time.clock()
    epochs = 2000
    steps = math.ceil(epochs * (len(images_train) / batch_size))
    print("Training started: {0} epochs in {1} steps\n".format(epochs, steps))
    classifier.train(input_fn=train_input_fn, steps=steps)
    end = time.clock()
    print("\nTraining finished after: {0:f} s\n".format(end - start))

    CarsDnnClassifier_Eval.eval(numClasses=numClasses)

if __name__ == '__main__':
    tf.app.run()