import tensorflow as tf
import numpy as np
import matplotlib.image as mpimg
import time
import math

import os
from os import listdir
from os.path import isfile, join

import constants as cn


def labelNeg(oneHot=False, discardNegatives=False):
    if discardNegatives and oneHot:
        return [0, 0, 0, 0, 0]
    elif oneHot:
        return [1, 0, 0, 0, 0, 0]
    else:
        return int(0)
def labelPos(oneHot=False, discardNegatives=False):
    if discardNegatives and oneHot:
        return [1, 0, 0, 0, 0]
    elif oneHot:
        return [0, 1, 0, 0, 0, 0]
    elif discardNegatives:
        return int(0)
    else:
        return int(1)
def labelGrass(oneHot=False, discardNegatives=False):
    if discardNegatives and oneHot:
        return [0, 1, 0, 0, 0]
    elif oneHot:
        return [0, 0, 1, 0, 0, 0]
    elif discardNegatives:
        return int(1)
    else:
        return int(2)
def labelTrees(oneHot=False, discardNegatives=False):
    if discardNegatives and oneHot:
        return [0, 0, 1, 0, 0]
    elif oneHot:
        return [0, 0, 0, 1, 0, 0]
    elif discardNegatives:
        return int(2)
    else:
        return int(3)
def labelRoads(oneHot=False, discardNegatives=False):
    if discardNegatives and oneHot:
        return [0, 0, 0, 1, 0]
    elif oneHot:
        return [0, 0, 0, 0, 1, 0]
    elif discardNegatives:
        return int(3)
    else:
        return int(4)

def labelFalsePositives(oneHot=False, discardNegatives=False):
    if discardNegatives and oneHot:
        return [0, 0, 0, 0, 1]
    elif oneHot:
        return [0, 0, 0, 0, 0, 1]
    elif discardNegatives:
        return int(4)
    else:
        return int(5)

def read_backgrounds_input(data_dir, takeFirst=None, oneHotLabels=False, discardNegatives= False):
    folder_name_suffix = str(cn.IMAGE_SIZE) + "x" + str(cn.IMAGE_SIZE)

    grass_dir = os.path.join(data_dir, "Grass/Segments" + folder_name_suffix)
    trees_dir = os.path.join(data_dir, "Trees/Segments" + folder_name_suffix)
    roads_dir = os.path.join(data_dir, "Roads/Segments" + folder_name_suffix)
    fp_dir = os.path.join(data_dir, "FalsePositives" + folder_name_suffix)

    filenamesGrass = [join(grass_dir, f) for f in listdir(grass_dir) if isfile(join(grass_dir, f)) if f.endswith('.png')]
    filenamesTrees = [join(trees_dir, f) for f in listdir(trees_dir) if isfile(join(trees_dir, f)) if f.endswith('.png')]
    filenamesRoads = [join(roads_dir, f) for f in listdir(roads_dir) if isfile(join(roads_dir, f)) if f.endswith('.png')]
    filenamesFp = [join(fp_dir, f) for f in listdir(fp_dir) if isfile(join(fp_dir, f)) if f.endswith('.png')]

    lblGrass = labelGrass(oneHotLabels, discardNegatives)
    lblTrees = labelTrees(oneHotLabels, discardNegatives)
    lblRoads = labelRoads(oneHotLabels, discardNegatives)
    lblFp = labelFalsePositives(oneHotLabels, discardNegatives)
    if takeFirst != None and takeFirst > 0:
        filenamesGrass = filenamesGrass[0:takeFirst]
        filenamesTrees = filenamesTrees[0:takeFirst]
        filenamesRoads = filenamesRoads[0:takeFirst]
        filenamesFp = filenamesFp[0:takeFirst]
    labels = np.array([lblGrass] * len(filenamesGrass) + [lblTrees] * len(filenamesTrees) + [lblRoads] * len(filenamesRoads) + [lblFp] * len(filenamesFp))
    filenames = filenamesGrass + filenamesTrees + filenamesRoads + filenamesFp

    images_raw = np.array([mpimg.imread(f) for f in filenames])
    return images_raw, labels

def read_cars_input(data_dir, isTest=False, takeFirst=None, onlyInclude=None, oneHotLabels=False, binaryLabel=False, discardNegatives=False):
    #label = tf.cast([label], tf.int32)
    folder_name_suffix = str(cn.IMAGE_SIZE) + "x" + str(cn.IMAGE_SIZE)
    if isTest:
        pos_dir = os.path.join(data_dir, "Testing", "Positives" + folder_name_suffix)
        neg_dir = os.path.join(data_dir, "Testing", "Negatives" + folder_name_suffix)
    else:
        pos_dir = os.path.join(data_dir, "Training", "Positives" + folder_name_suffix)
        neg_dir = os.path.join(data_dir, "Training", "Negatives" + folder_name_suffix)

    if binaryLabel and oneHotLabels:
        lblPos = [0, 1]
        lblNeg = [1, 0]
    else:
        lblPos = labelPos(oneHotLabels, discardNegatives)
        lblNeg = labelNeg(oneHotLabels, discardNegatives)

    if onlyInclude == None:
        filenamesPos = [join(pos_dir, f) for f in listdir(pos_dir) if isfile(join(pos_dir, f)) if f.endswith('.png')]
        filenamesNeg = [join(neg_dir, f) for f in listdir(neg_dir) if isfile(join(neg_dir, f)) if f.endswith('.png')]
        if takeFirst != None and takeFirst > 0:
            filenamesNeg = filenamesNeg[0:takeFirst]
            filenamesPos = filenamesPos[0:takeFirst]
        labels = np.array([lblPos] * len(filenamesPos) + [lblNeg] * len(filenamesNeg))
        filenames = filenamesPos + filenamesNeg
    elif onlyInclude:
        filenamesPos = [join(pos_dir, f) for f in listdir(pos_dir) if isfile(join(pos_dir, f)) if f.endswith('.png')]
        if takeFirst != None and takeFirst > 0:
            filenamesPos = filenamesPos[0:takeFirst]
        labels = np.array([lblPos]*len(filenamesPos))
        filenames = filenamesPos
    else:
        filenamesNeg = [join(neg_dir, f) for f in listdir(neg_dir) if isfile(join(neg_dir, f)) if f.endswith('.png')]
        if takeFirst != None and takeFirst > 0:
            filenamesNeg = filenamesNeg[0:takeFirst]
        labels = np.array([lblNeg] * len(filenamesNeg))
        filenames = filenamesNeg

    images_raw = np.array([mpimg.imread(f) for f in filenames])

    # filenames = [os.path.join(data_dir, 'train_data_pos.bin')]
    # if not tf.gfile.Exists(filenames[0]):
    #     raise ValueError('Failed to find file: ' + filenames)
    for f in filenames:
        if not tf.gfile.Exists(f):
            raise ValueError('Failed to find file: ' + f)

    return images_raw, labels

def read_input(data_dir):
    filenames = [join(data_dir, f) for f in listdir(data_dir) if isfile(join(data_dir, f)) if f.endswith('.png')]

    for f in filenames:
        if not tf.gfile.Exists(f):
            raise ValueError('Failed to find file: ' + f)

    return np.array([mpimg.imread(f) for f in filenames])