import tensorflow as tf
import numpy as np
import matplotlib.image as mpimg
import time
import CarsDnnClassifier
import os
from os import listdir
from os.path import isfile, join
import constants as cn

import input_handler as input

def eval(numClasses=cn.NUM_CLASSES):
    data_dir = "/CarCounterData/CowcData/"
    images_test, labels_test = input.read_cars_input(data_dir, isTest=True, takeFirst=None, onlyInclude=True)

    classifier = CarsDnnClassifier.get_classifier(numClasses=numClasses)

    # Define the test inputs
    test_input_fn = tf.estimator.inputs.numpy_input_fn(
        x={"x": images_test},
        y=labels_test,
        num_epochs=1,
        shuffle=False,
        queue_capacity=5000,
        num_threads=1,
    )

    # Evaluate accuracy
    accuracy_score = classifier.evaluate(input_fn=test_input_fn)["accuracy"]
    print("\nTest Accuracy: {0:f}%\n".format(accuracy_score * 100))

def main(argv=None):
   eval()

if __name__ == '__main__':
    tf.app.run()