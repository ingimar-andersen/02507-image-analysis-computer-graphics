import argparse
import sys
import constants as cn
import cars_deep

import tensorflow as tf

import time
import input_handler as input

import os
import math
import contextlib

FLAGS = None
model_dir = "/CarCounterData/CowcData/Models" + str(cn.IMAGE_SIZE)
if cn.IS_TRAINING:
    base_dir = "/CarCounterData/CowcData/Training/Modified/"
    file_name = "03747(7532,1288)(9532,3288)"
else:
    base_dir = "/CarCounterData/CowcData/Testing/Modified/"
    #file_name = "12TVL240120(3532,9400)(5334,11245)"
    file_name = "12TVL160640-CROP(5200,3850)(6200,4850)"

prefix = "Segments" + str(cn.IMAGE_SIZE) + "x" + str(cn.IMAGE_SIZE) + "_"
file_name = prefix + file_name

data_dir = os.path.join(base_dir, file_name)
prediction_filepath = os.path.join(data_dir, "predictions.txt")

def predict():
    # Import data
    images_test = input.read_input(data_dir)

    # Create the model
    x = tf.placeholder(tf.float32, [None, cn.IMAGE_SIZE, cn.IMAGE_SIZE, cn.IMAGE_BANDS])

    # Build the graph for the deep net
    y_conv, keep_prob = cars_deep.deepnn(x)

    with tf.name_scope('prediction'):
        prediction_op = tf.argmax(y_conv, 1)
        y_prob = tf.nn.softmax(y_conv,1)

    with tf.Session() as sess:
        ckpt = tf.train.get_checkpoint_state(model_dir)
        if ckpt and ckpt.model_checkpoint_path:
            # Restores from checkpoint
            saver = tf.train.Saver(tf.trainable_variables(),max_to_keep=0)
            saver.restore(sess, ckpt.model_checkpoint_path)
            # Assuming model_checkpoint_path looks something like:
            #   /my-favorite-path/model.ckpt-0,
            # extract global_step from it.
            global_step = ckpt.model_checkpoint_path.split('/')[-1].split('-')[-1]
        else:
            print('No checkpoint file found')
            return
        sess.run(tf.global_variables_initializer())
        start = time.clock()
        print_every = 1000
        batch_size = 50
        iterations = int(math.floor(len(images_test)/batch_size))

        # Remove previous predictions
        with contextlib.suppress(FileNotFoundError):
            os.remove(prediction_filepath)
        with open(prediction_filepath, 'a') as file:
            index = 0
            for i in range(iterations):
                end_index = index + batch_size
                batch = [images_test[index:end_index]] #, labels_test[index:end_index]]
                index = end_index
                y_conv_out, y_prob_out, pred_out = sess.run([y_conv, y_prob, prediction_op], feed_dict={x: batch[0], keep_prob: 1.0})

                # Write results to file
                for i in range(len(pred_out)):
                    cat = int(pred_out[i])
                    prob = y_prob_out[i].max()
                    file.write("{0:d} {1:f}\n".format(cat, prob))

                if i % print_every == 0:
                    end = time.clock()
                    elapsed = end - start
                    start = end
                    print('calc time per batch: {0:.4f} s'.format(elapsed / print_every))

        #prediction_result = prediction_op.eval(feed_dict={x: batch[0], keep_prob: 1.0})
        #y_conv_out, y_prob_out, pred_out = sess.run([y_conv, y_prob, prediction_op], feed_dict={x: batch[0], keep_prob: 1.0}) # nemmere med dict som input output
        #prediction_results = sess.run(fetches={'prediction': prediction_op, 'probabilities': y_prob}, feed_dict={x: batch[0], keep_prob: 1.0})  # nemmere med dict som input output
        #predicted_classes = [[p['prediction'], p['probabilities'].max()] for p in prediction_results]
        print("prediction complete")

def main(_):
    predict()

if __name__ == '__main__':
  dir_path = os.path.dirname(os.path.realpath(__file__))
  parser = argparse.ArgumentParser()
  FLAGS, unparsed = parser.parse_known_args()
  tf.app.run(main=main, argv=[sys.argv[0]] + unparsed)