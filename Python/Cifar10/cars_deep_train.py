from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import random
import sys
import constants as cn
import tempfile
import os
import time
import datetime
import math
import input_handler as input

import tensorflow as tf

import numpy as np
import matplotlib.image as mpimg
import time

import os
import cars_deep
from os import listdir
from os.path import isfile, join

data_dir = "/CarCounterData/CowcData/"
train_dir = data_dir + "Models" + str(cn.IMAGE_SIZE)
if not os.path.exists(train_dir):
    os.makedirs(train_dir)

FLAGS = None

print_steps = 1000
test_steps = 5000
batch_size = 128
one_hot_label = True
discardNegatives = False

global_step = 0


def train(shuffle=True):
    # Import data
    # mnist = input_data.read_data_sets(FLAGS.data_dir, one_hot=True)
    index = 0
    global_step = 0

    # Create the model
    x = tf.placeholder(tf.float32, [None, cn.IMAGE_SIZE, cn.IMAGE_SIZE, cn.IMAGE_BANDS])

    # Define loss and optimizer
    y_ = tf.placeholder(tf.float32, [None, cn.NUM_CLASSES])

    # Build the graph for the deep net
    y_conv, keep_prob = cars_deep.deepnn(x)

    if cn.NUM_CLASSES == 2:
        binary_label = True
        images_train, labels_train = input.read_cars_input(data_dir, isTest=False, takeFirst=None,
                                                                     onlyInclude=None, oneHotLabels=one_hot_label,
                                                                     binaryLabel=binary_label,
                                                                     discardNegatives=discardNegatives)
    else:
        binary_label = False
        images_train_cars, labels_train_cars = input.read_cars_input(data_dir, isTest=False, takeFirst=None, onlyInclude=None, oneHotLabels=one_hot_label, binaryLabel=binary_label, discardNegatives=discardNegatives)
        # images_train1, labels_train1 = input.read_cars_input(data_dir, isTest=False, oneHotLabels=one_hot_label)
        backgrounds_dir = "/CarCounterData/CowcData/Training/"
        images_train_backg, labels_train_backg = input.read_backgrounds_input(backgrounds_dir, takeFirst=None, oneHotLabels=one_hot_label, discardNegatives=discardNegatives)
        images_train = np.concatenate([images_train_cars, images_train_backg], 0)
        labels_train = np.concatenate([labels_train_cars, labels_train_backg], 0)

    images_test, labels_test = input.read_cars_input(data_dir, isTest=True, takeFirst=None, onlyInclude=True, oneHotLabels=one_hot_label, binaryLabel=binary_label, discardNegatives=discardNegatives)

    max_index = images_train.shape[0]
    steps = FLAGS.epochs * math.ceil(1.0 * max_index / batch_size)

    with tf.name_scope('loss'):
        label_scalars = tf.argmax(y_, 1)
        cross_entropy = tf.nn.sparse_softmax_cross_entropy_with_logits(labels=label_scalars, logits=y_conv)
        #cross_entropy = tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y_conv)
    cross_entropy = tf.reduce_mean(cross_entropy)

    with tf.name_scope('adam_optimizer'):
        learning_rate = 1e-4
        train_step = tf.train.AdamOptimizer(learning_rate=learning_rate, beta1=0.9, beta2=0.999, epsilon=1e-8).minimize(cross_entropy)

    with tf.name_scope('accuracy'):
        correct_prediction = tf.equal(tf.argmax(y_conv, 1), tf.argmax(y_, 1))
        correct_prediction = tf.cast(correct_prediction, tf.float32)
    accuracy = tf.reduce_mean(correct_prediction)

    with tf.name_scope('prediction'):
        predicted_cat = tf.argmax(y_conv, 1)
        actual_cat = tf.argmax(y_, 1)
        probabilities = tf.nn.softmax(y_conv, 1)

    # Create a saver.
    saver = tf.train.Saver(tf.global_variables())

    def shuffle_train():
        combined = list(zip(images_train, labels_train))
        random.shuffle(combined)
        images_train[:], labels_train[:] = zip(*combined)

    def shuffle_test():
        combined = list(zip(images_test, labels_test))
        random.shuffle(combined)
        images_test[:], labels_test[:] = zip(*combined)

    if shuffle:
        shuffle_train()

    config = tf.ConfigProto()
    config.gpu_options.allocator_type = 'BFC'

    with tf.Session(config=config) as sess:
        sess.run(tf.global_variables_initializer())
        start = time.clock()
        def test_all() :
            test_start = time.clock()
            test_batch_size = 128  # int(math.floor(len(labels_test) / test_parts))
            accuracy_result = []
            step_test = 0
            end_index_test = 0
            max_index_test = images_test.shape[0]
            while step_test < max_index_test:
                end_index_test = step_test + test_batch_size
                if end_index_test <= max_index_test:
                    predicted_cat_out, actual_cat_out, probabilities_out, accuracy_out = sess.run(
                        [predicted_cat, actual_cat, probabilities, accuracy],
                        feed_dict={x: images_test[step_test:end_index_test],
                                   y_: labels_test[step_test:end_index_test],
                                   keep_prob: 1.0})
                    accuracy_result.append(accuracy_out)

                step_test = end_index_test

            test_elapsed = time.clock() - test_start
            print("test accuracy {0:.4f} - calc time: {1:.4f} s".format(sum(accuracy_result) / len(accuracy_result), test_elapsed))

        start_training = time.clock()
        for i in range(steps):
            global_step = i + 1 # step starts from zero
            end_index = index + batch_size
            if end_index > max_index:
                if shuffle:
                    shuffle_train()
                index = 0
                end_index = batch_size
            batch = [images_train[index:end_index], labels_train[index:end_index]]
            index = end_index
            train_step.run(feed_dict={x: batch[0], y_: batch[1], keep_prob: 0.5})
            if global_step % print_steps == 0:
                end = time.clock()
                elapsed = end - start
                start = end

                time_per_batch = elapsed / print_steps
                remaining_seconds = math.ceil((steps - global_step)*time_per_batch)

                secs = remaining_seconds%60
                mins = math.floor(remaining_seconds/60)%60
                hours = math.floor(remaining_seconds/60/60)%60

                print('step %d' % (global_step))
                print('calc time per batch/step: {0:.4f} s'.format(time_per_batch))
                print('{0:.2f}% complete\n'.format(1.0 * global_step / steps * 100))
                print('remaining {0:02d}:{1:02d}.{2:02d}'.format(hours, mins, secs))

            if global_step % test_steps == 0:
                test_all()
            # Save the model checkpoint periodically.
            if global_step % 1000 == 0 or global_step == steps:
                checkpoint_path = os.path.join(train_dir, 'model.ckpt')
                saver.save(sess, checkpoint_path, global_step=global_step)

        end_training = time.clock()
        print('Total training time: {0:.4f} s'.format(end_training - start_training))

def main(_):
    train()

if __name__ == '__main__':
    dir_path = os.path.dirname(os.path.realpath(__file__))
    parser = argparse.ArgumentParser()
    parser.add_argument('--epochs', type=int,
                        default=100,
                        help='Number of epochs for the training procedure')
    FLAGS, unparsed = parser.parse_known_args()
    tf.app.run(main=main, argv=[sys.argv[0]] + unparsed)