# Preparing a TF model for usage in Android
# By Omid Alemi - Jan 2017
# Works with TF <r1.0

import sys
import os
import tensorflow as tf
from tensorflow.python.tools import freeze_graph
from tensorflow.python.tools import optimize_for_inference_lib
from google.protobuf import text_format

MODEL_NAME = 'cars'

# Freeze the graph
train_dir = "/tmp/cifar10_train"
input_graph_path = os.path.join(train_dir, 'graph.pbtxt')
checkpoint_path = os.path.join("./", train_dir, 'model.ckpt-10000')
input_saver_def_path = ""
input_binary = False
output_node_names = "softmax" #"softmax"
restore_op_name = "save/restore_all"
filename_tensor_name = "save/Const:0"
output_frozen_graph_name = os.path.join(train_dir,'frozen_'+MODEL_NAME+'.pb')
output_optimized_graph_name = os.path.join('optimized_'+MODEL_NAME+'.pb')

# ensure files exist, create new empty of not
open(output_frozen_graph_name, 'a').close()
open(output_optimized_graph_name, 'a').close()

clear_devices = True

graph_def = tf.GraphDef()
with open(input_graph_path, "rb") as f:
    if(input_binary):
        graph_def.ParseFromString(f.read())
    else:
        text_format.Merge(f.read(), graph_def)

def _node_name(n):
  if n.startswith("^"):
    return n[1:]
  else:
    return n.split(":")[0]

for node in graph_def.node:
    with open(os.path.join(train_dir, 'node_names.txt'), 'a') as file:
        n = _node_name(node.name)
        file.write(n + "\n")

freeze_graph.freeze_graph(input_graph_path, input_saver_def_path,
                          input_binary, checkpoint_path, output_node_names,
                          restore_op_name, filename_tensor_name,
                          output_frozen_graph_name, clear_devices, "")



# Optimize for inference

input_graph_def = tf.GraphDef()
with tf.gfile.Open(output_frozen_graph_name, "r") as f:
    data = f.read()
    input_graph_def.ParseFromString(data)


output_graph_def = optimize_for_inference_lib.optimize_for_inference(
        input_graph_def,
        # ["I"], # an array of the input node(s)
        # ["O"], # an array of output nodes
        ["conv1"],  # an array of the input node(s)
        ["O"],  # an array of output nodes
        tf.float32.as_datatype_enum)


# Save the optimized graph

f = tf.gfile.FastGFile(output_optimized_graph_name, "w")
f.write(output_graph_def.SerializeToString())


